## [KiCad 安装](./KiCad_install.md)

## [KiCad 文档](./KiCad_doc.md)

### KiCad 在线文档

[KiCad 在线简体中文文档](http://docs.kicad-pcb.org/master/zh/) 目前简体中文文档和 i18n 由 [kicad-cn](https://github.com/kicad-cn) 和 [taotieren](https://github.com/taotieren) 提供翻译和维护。

### KiCad 离线文档

- [KiCad 文档简体中文](https://gitee.com/KiCAD-CN/KiCad-doc_build)仓库，由于 Gitee 仓库大小限制目前只提供编译完后英文原版和简体中文文档下载。
- [KiCad 文档简体中文 PDF 下载](https://gitee.com/KiCAD-CN/KiCad-doc_build/releases) 发行版只提供 PDF 文档，需要其他格式文档请自行到仓库对应目录下载。
- 因为 KiCad 在线文档的 CI/CD 服务所使用的是 Ubuntu 14.04 系统，和我本地使用的 Manjaro Linux 编译系统不一致，会存在简体中文文档编译后显示不全，甚至部分表格排序错误等问题，目前正在和 KiCad 文档团队沟通此问题，暂时提出使用多种不同编译环境来编译文档，最后发行版本使用哪个编译环境还没确定。也有提出更新文档编译系统，也有提出更新文档编译方式，因为编译的文档是通过 latex 来进行的。目前的 latex 是使用 Python2 开发，现在 Python 官方将放弃 Python2 的支持。
- 如果需要阅读简体中文文档前使用离线文档阅读或校对，如果在文档阅读中遇到不明白或者更好建议都可以到仓库下提交建议或者问题。
- 每次的简体中文文档发行版都会提供相应的修复和更新说明。

## [KiCad 插件](./KiCad_plugin.md)