# KiCad-CN-Forum

#### 介绍
KiCad 中国讨论

KiCad 中文论坛：https://www.ilovekicad.com/

KiCad 开源中国 ① 群：742339528 (已满员，~~群主失联中，暂时无法扩容群人数。~~)

![KiCad_742339528](./KiCad_742339528.png)

KiCad 开源中国 ② 群：290150427

PC QQ 二维码解析：


![KiCad_开源中国②](./KiCad_开源中国②.png)

手机 QQ 扫码：


![KiCad_开源中国②_290150427](./KiCad_开源中国②_290150427.jpg)

Telegram 简体中文交流群: https://t.me/KiCad_zh_CN

飞书交流群：

![KiCad_CN_feishu](./KiCad_CN_feishu.png)

飞书话题谈论：

![Talk_KiCad_CN_feishu](./Talk_KiCad_CN_feishu.png)

KiCad 发行版 Windows 版本阿里云盘下载链接：[KiCad 发行版](https://www.aliyundrive.com/s/yZcznhyJg3Z)

#### [KiCad 简介](./zh_CN/KiCad_description.md)

#### [KiCad 安装](./zh_CN/KiCad_install.md)

#### [KiCad 文档](./zh_CN/KiCad_doc.md)

#### [KiCad 插件](./zh_CN/KiCad_plugin.md)

#### [KiCad 中国群文件](./zh_CN/KiCad_CN_group_files.md)

#### [KiCad 与其他 EDA 转换](./zh_CN/KiCad_2_other_EDA.md)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
