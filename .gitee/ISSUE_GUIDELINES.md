## 1. 在你开始之前...

对于使用 KiCad 时遇到的问题，请先访问 Gitlab 上的 [KiCad Bug 记录](https://gitlab.com/kicad/code/kicad/issues) 搜索相关 Bug。

填写问题内容前，请选择合适的类型，然后根据模板补全内容，方便社区人员理解。