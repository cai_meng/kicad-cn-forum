#!/usr/bin/env bash

output_file="目录文件及文件夹预览生成脚本.md"
if [ -f /usr/bin/tree ];then
	echo '```bash' > $output_file
	tree >> $output_file
    if [ -f /usr/bin/sed ];then
        #sed '/^ *$/d' -i $output_file
        sed '$d' -i $output_file
    else
        echo 'Please check if there is sed under the /usr/bin path? Does not exist. Please install sed.'
    fi
	echo '```' >> $output_file
else
    echo 'Please check if there is tree under the /usr/bin path? Does not exist. Please install tree.'
fi

