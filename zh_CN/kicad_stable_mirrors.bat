@echo off

chcp 65001

echo ## [KiCad 中国群文件](./KiCad_CN_group_files.md) > KiCad_downloads.md

tree /f >>  目录文件及文件夹预览.md

echo ``` >>  目录文件及文件夹预览.md

REM sed -i "2,4d"  FileReview.md

move 目录文件及文件夹预览.md  FileReview.md 

rem 根据指定的行号范围删除多个txt文件里的连续多行内容
set #=Any question&set @=WX&set/az=0x152c&set $$=cel
title %#% +%$%%$%/%@% %z%
cd /d "%~dp0"
set lines=2-4
REM set "newfolder=newfolder"
chcp 936
REM if not exist "%newfolder%" md "%newfolder%"
for /f "delims=" %%a in ('dir /a-d/b FileReview.md') do (
    echo;"%%a"
    (for /f "tokens=1,2 delims=-" %%b in ("%lines%") do (
        for /f "tokens=1* delims=:" %%e in ('findstr /n .* "%%a"') do (
            if %%e lss %%b echo;%%f
            if %%e gtr %%c echo;%%f
        )
    ))>"tmp.md"
	REM "%newfolder%\%%a"
)
echo;%#% +%@% %$$%%z%

del FileReview.md 

chcp 65001

move tmp.md 目录文件及文件夹预览.md
