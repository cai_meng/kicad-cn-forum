@[toc]

## [KiCad 文档](./KiCad_doc.md)

## [KiCad 插件](./KiCad_plugin.md)

### 常用插件整理

编号|                    插件名称                |      插件介绍         |     插件状态     |插件支持的操作系统|备注
:--:|:-------------------------------------------|:----------------------|:----------------:|:----------------:|----
1	|BezierTrack								 | 贝塞尔曲线        	 |       			|                  |
2	|CircularZone								 | 圆型禁止布线或覆铜    |     				|                  |
3	|diffpads									 | 差分线圆滑处理        |     				|                  |
4	|flexRoundingSuite	VIA/PAD/Track 			 | 三个工具集合          |     				|                  |
5	|HierPlace									 | 整理零件分类并整齐摆放|     				|                  |
6	|InteractiveHtmlBom						 	 | 生成动态 BOM          |     				|                  |
7	|kicad_align								 | 对齐焊盘        		 |     				|                  |
8	|kicad_text_tool							 | 轮廓字体文本放置工具  |  5.1.9 不可用    |                  |
9	|kicad_tools								 |KiCad 生产文件生成器   |       			|                  |
10	|kicad-action-scripts						 |圆形覆铜和过地孔集合   |       			|                  |
11	|kicad-round-tracks			 				 | 弧线布线插件 		 |       			| Windows 不可用   |
12	|RF-tools									 |射频插件         		 |       			|                  |
13	|spiki						 				 |PCB 螺旋电感线生成器   |       			|                  |
14	|teardrops					 				 |泪滴         			 |       			|                  |
15	|toolbox									 |工具箱         		 |       			|                  |
16	|ViaStitching					 			 |批量过孔        		 |       			|                  |
17	|WireIt						 				 |直接在 PCB 中手工画导线|       			|                  |



### 扩展工具

## [获取更多 KiCad 插件介绍](https://www.eetree.cn/doc/detail/1568) 

#### [FreeRouting 自动布线工具](https://freerouting.org/)

简介：

- 是旨在但不限于印刷电路板（PCB）的布线软件。
- 通过使用标准 Specctra 或 Electra DSN 接口，它可以与许多 CAD 工具一起使用，包括 LayoutEditor，KiCad 和 Eagle。
- 它导入由 Specctra 接口生成的 DSN 文件并导出 Specctra 会话文件。
- FreeRouting 由 Alfons Wirtz 于 2004 年开发。
- 他于 2008 年停止开发，并于 2014 年根据 GPL 发布了源代码。
- 从 2015 年开始，FreeRouting 包含在 [LayoutEditor](https://layouteditor.com/) 软件包中，其源代码由其团队维护。最新的源代码，手册和所有其他相关信息可以在此网页上找到。

[和 KiCad 一起使用要求](https://freerouting.org/freerouting/using-with-kicad)：

- 要运行 FreeRouting，需要在 FreeRouting 可执行文件旁边添加一个 Java 运行时环境。默认情况下，许多系统应该已经安装了它。如果您的系统上缺少它，则可以从 Oracle Homepage下载它。
- 安装 FreeRouting 可执行文件的最简单方法是安装 [LayoutEditor 软件包](https://layouteditor.com/download)。它在 LayoutEditor 程序包的 bin 文件夹中包含一个已准备好使用的 FreeRouting 。要启动 FreeRouting 应用程序，只需按 freeRouting.jar 文件。或者，您可以从此网页下载 FreeRouting 源文件，并使用 Java Developer Tools 进行编译。

``` bash
// 在 LayoutEditor 安装目录下使用命令行运行 freerouting.jar
java -jar freerouting.jar
```

用法：

- 在 PCBnew 工具中，请按下自动布线按钮
- Freerouting 工具的对话框将打开。在此对话框中，您可以导出光绘文件（.DSN）。这是 FreeRouting 需要的输入文件。打开 FreeRouting 后，打开此文件并执行布线。布线完成后，将结果存储到光绘会话文件（.SES）。在同一个 KiCad 对话框中，可以导入带有布线结果的文件。该对话框还包含使用 FreeRouting 的附加帮助。


#### [kicadStepUpMod](https://github.com/easyw/kicadStepUpMod/)

KiCad StepUp 工具是 FreeCAD 宏和 FreeCAD WorkBench，用于帮助 KiCad EDA 和 FreeCAD 或机械 CAD 之间的机械协作。

功能：

- 在 FreeCAD 中加载 KiCad 板和零件并将其导出到 STEP（或 IGES）以进行完整的 ECAD MCAD 协作
- 在 FreeCAD 中加载 kicad_mod 封装，以便轻松精确地将机械模型与 kicad 封装对齐
- 使用材料属性将零件，板，外壳的 STEP 3D 模型转换为 VRML，以便在 kicad 中得到最佳使用
- 检查外壳和封装设计的干扰和碰撞
- 使用 FreeCAD Sketcher 设计一个新的 pcb Edge 并将其推送到现有的 kicad_pcb 板
- 从 kicad_pcb 板中拉出 pcb Edge，在 FC Sketcher 中编辑它并将其推回 kicad
- 在 FreeCAD 中设计一个新的封装，以在封装中获得 Sketch 的力量
- 生成 Blender 兼容的 VRML 文件

#### [KiCost](https://github.com/xesscorp/KiCost)

KiCost 旨在作为一个脚本运行，用于为使用 KiCad 开发的电路板生成部分成本电子表格。除命令行外，KiCost 还带有图形用户界面。

特征:

- 通过 pip 轻松安装（参见文档文件夹）。
- 处理来自 KiCad 原理图的 BOM XML 文件，通过设置几个流行的分销商 Web 服务器的价格和库存数据来创建零件成本电子表格。（您也可以输入自己的数量调整后的定价数据，用于专门的零件或在支持的经销商处找不到的。）;
- 处理来自 Altium，Proteus，Eagle，Upverter 和手工制作的 CSV 的 BOM 文件;
- 电子表格包含每个经销商对单个元件和总板的数量调整定价;
- 输入要在电子表格单元格中构建的板数，并更新总板和各个元件的所有定价;
- 电子表格还显示每个经销商的每个元件的现有库存;
- 输入您要从每个经销商处购买的每个元件的数量，元件编号和数量列表将以您可以直接剪切并粘贴到每个经销商的网站订购页面的格式显示。

#### [altium2kicad](https://github.com/thesourcerer8/altium2kicad)

用于 PCB 和原理图的 Altium 到 KiCad 转换器。

最新的 KiCad 测试版本已经实现 AD PCB 导入功能。`https://gitlab.com/kicad/code/kicad/-/merge_requests/60`
可以通过下载最新的 夜间测试版 体验功能。


Arch Linux 和 Manjaro Linux 通过 AUR 安装：

``` bash
// 能力越大，责任越大。明白每一行命令是什么意思再执行。
// 搜索 altium2kicad-git
yaourt -Ss altium2kicad-git

// 安装 altium2kicad-git
yaourt -S altium2kicad-git

// 需要从 GitHub 仓库下载源码。下载速度可能会收到影响。这里建议使用 proxychains 套一层代理来安装。
// 安装 proxychains-ng 软件包
pacman -S proxychains-ng 

// 修改 /etc/proxychainas.conf 
sudo vim /etc/proxychains.conf

// 在最后面新增一行代理地址 支持 http/https/socks5 根据实际情况配置如：
http    127.0.0.1:8080
https    127.0.0.0:8443
socks5    127.0.0.1:1080

// 修改后保存退出。使用方式在原本命令行前加上 proxychains 就可以走自定义代理。并自行根据实际情况进行测试。
proxychains curl www.google.com/www.github.com

// 使用 proxychains 下载 AUR 包
proxychains yaourt -S altium2kicad-git

```

系统要求：Perl 可选：https://github.com/cbernardo/kicad_oce_vis 或 https://github.com/twlostow/step2wrl 或 FreeCAD 要将 Altium 项目转换为 KiCad：

该软件也作为在线服务提供：http://www2.futureware.at/KiCad/

转到包含 .PcbDoc 和 .SchDoc 文件的目录并运行：

- unpack.pl（它将 .PcbDoc 和 .SchDoc 文件解压缩到子目录中）
- 如果 Altium 设计包含步进格式的 3D 模型，请运行 kicadd_oce_vis 或 step2wrl 或 FreeCAD 将文件转换为 wrl（打开 FreeCAD 并执行宏 step2wrl.FCMacro）
- convertedchema.pl（它将原理图从子目录转换为 .sch 和 -cache.lib 文件）
- convertpcb.pl（它将PCB转换为 .kicad_pcb 文件）

由于 Altium 和 KiCad 之间的巨大差异，弱文件格式文档和文件格式的高复杂性，此转换器无法保证转换的质量。请验证转换器的输出如果此转换器不适用于您的文件，请随时提供您的文件和屏幕截图，了解它们的外观和样式，我将尽力提供帮助。

此转换器的当前限制：

- 不转换设计规则检查设置

目前已知的 KiCad 限制：

- 组件符号的贝塞尔曲线 -> WONTFIX -> 线性化的解决方法
- 多行文本框架
- 具有多个水平线排列为三角形的 GND 符号
- 单个对象的单独颜色，如线条，......
- 椭圆
- 圆角矩形
- 椭圆弧
- 软硬结合
- 不支持八角焊盘
- 线宽大于 Altium 设计半径的弧线会破坏 VRML 输出
- STEP（STP）文件支持 -> 将来会修复，中间解决方法：使用 stp2wrl 或 FreeCAD 进行转换

#### [KiCad 生产文件生成器](https://github.com/xtoolbox/kicad_tools)

KiCad 绘图工具 Fork 自 https://github.com/blairbonnett-mirrors/kicad/blob/master/demos/python_scripts_examples/gen_gerber_and_drill_files_board.py

S-Expression 解析工具 Fork 自 https://github.com/tkf/sexpdata

使用方法:

适用于：KiCad EDA 5.1.0 +

1. 下载插件，使用以下命令

```bash
Windows 安装：

git clone https://github.com/xtoolbox/kicad_tools.git %appdata%/kicad/scripting/plugins/kicad_tools

Linux 安装：

git clone https://github.com/xtoolbox/kicad_tools.git ~/kicad/scripting/plugins/kicad_tools

或：

git clone https://github.com/xtoolbox/kicad_tools.git ~/.kicad_plugins/kicad_tools

Mac OS 安装：

git clone https://github.com/xtoolbox/kicad_tools.git ~/Library/Application Support/kicad/scripting/plugins/kicad_tools

新版 Mac OS：

git clone https://github.com/xtoolbox/kicad_tools.git ~/Library/Preferences/kicad/scripting/plugins/kicad_tools

```

2. 或者在[工具]->[外部工具]下执行 Gen Manufacture Docs 命令。

3. BOM 文件和位置文件会以 CSV 格式存放在电路板相同目录下，Gerber 和钻孔文件放在电路板目录下的 Gerber 目录中。通过此方法生成的钻孔文件中的槽孔会被转换成多个普通孔。

*注意*:

GenMFDoc() 会改变电路板的钻孔原点。建议先用 GenMFDoc() 生成 BOM 文件和位置文件，再生成 Gerber 文件。
生成的 BOM 文件和坐标文件以及 Gerber 和钻孔文件可以直接在 sz-jlc.com 进行贴装

#### [PCAD/AD库文件转换KiCad工具](https://github.com/xtoolbox/pcad2kicad)

特点:

- Altium Designer 的二进制原理图库转换成 KiCad 格式
- PCAD 的 ASCII 原理图库转换成 KiCad 格式
- PCAD 的 ASCII 封装图库转换成 KiCad 格式

##### ad2kicad

需要 [7z](https://www.7-zip.org/download.html) 和 [lua 5.3](https://sourceforge.net/projects/luabinaries/files/5.3.4/)

单个模式：

```bash
lua ad2kicad.lua <输入文件名> [输出文件名] [fpLib]
```

批处理模式：

```bash
lua ad2kicad.lua --batch <输入路径> [输出路径] [fpLib] [prefix] [O1 = N1 [O2 = N2 ...]]
```
##### pcad2kicad

需要 [lua 5.3](https://sourceforge.net/projects/luabinaries/files/5.3.4/)

单个模式：

```bash
lua pcad2kicad.lua <输入文件名> [输出文件名] [输出路径] [fpLib]
```

批处理模式：

```bash
lua pcad2kicad.lua --batch <输入路径> [输出路径] [fpLib] [prefix] [O1 = N1 [O2 = N2 ...]]
```

#### [WireIt 插件](https://github.com/xesscorp/WireIt)

这个 PCBNEW 插件允许您在 PCB 上的焊盘之间添加电线（或网络），删除它们，并在焊盘之间交换电线。在进行 FPGA 等高引脚数封装的布局时，这有助于物理连接多组相关引脚。

- 免费软件：MIT 许可证

##### 特征：

- 将两个或多个焊盘彼此连接或连接到现有网。
- 从网上取下一个或多个焊盘。
- 交换连接两个焊盘的网。
- 输出包含对网表所做更改的文件。

##### 安装：

只需将 WireIt.py 文件和 WireIt_icons 目录复制到以下目录之一：

```bash
Windows 安装：

git clone https://github.com/xesscorp/WireIt.git %appdata%/kicad/scripting/plugins/WireIt

Linux 安装：

git clone https://github.com/xesscorp/WireIt.git ~/kicad/scripting/plugins/WireIt

或：

git clone https://github.com/xesscorp/WireIt.git ~/.kicad_plugins/WireIt

Mac OS 安装：

git clone https://github.com/xesscorp/WireIt.git ~/Library/Application Support/kicad/scripting/plugins/WireIt

新版 Mac OS：

git clone https://github.com/xesscorp/WireIt.git ~/Library/Preferences/kicad/scripting/plugins/WireIt

```

*注意*

如果在 *plugins* 目录下使用目录安装，无法使用的话，请手动将相关文件移动到 *plugins* 目录下。
手动重启软件后即可使用。
如下：

```bash
Windows 下调整目录：

mv %appdata%/kicad/scripting/plugins/WireIt/*  %appdata%/kicad/scripting/plugins/ 
del  %appdata%/kicad/scripting/plugins/WireIt/

Linux 下调整目录：

mv ~/kicad/scripting/plugins/WireIt/*  ~/kicad/scripting/plugins/
rm -r ~/kicad/scripting/plugins/WireIt
```

##### 用法：

按下 *工具* => *扩展插件* => *WireIt* 按钮启动插件。这为四个 WireIt 工具中的每个工具添加了一个按钮到 PCBNEW 窗口：

##### WireIt 工具：

该工具将焊盘连接到网。它使用如下：

1. 使用 shift 键单击鼠标操作，在 PCB 上选择一个或多个焊盘，布线轨道或区域。
2. 单击按钮。
单击 WireIt 按钮后，将发生以下任一情况：

- 如果所有焊盘都未连接，则会出现一个对话窗口，您可以在其中键入将要连接它们的新网络的名称，或选择现有网络的名称。按下 OK 按钮将导致所选焊盘之间出现空气线。按下 Cancel 将中止导线的创建。
- 如果所有焊盘/轨道/区域已连接到同一网络，则会出现一个对话窗口，允许您输入该网络的新名称，或选择现有的网络名称。然后原始网上的那些焊盘/轨道/区域和任何其他焊盘/轨道/区域 将被移动到新网。
- 如果一些焊盘/轨道/区域已连接到单个网络而其余焊盘未连接，则未连接的焊盘将被添加到该网络。没有用于命名网络的对话窗口，因为它已有名称。
- 如果两个或多个焊盘/轨道/区域已经连接到不同的网络，那么这些网络将被合并，并且这些网络上的所有焊盘/轨道/区域将被移动到合并的网络。将出现一个对话窗口，允许您选择合并网络的名称。

##### CutIt 工具：

此工具可从网络中移除一个或多个焊盘。它使用如下：

1. 使用 shift 键单击鼠标操作选择 PCB 上的一个或多个焊盘。
2. 单击按钮。
点击 CutIt 按钮后，任何连接到选定焊盘的电线都将被移除，焊盘将不再连接。

##### SwapIt 工具

该工具交换连接到两个焊盘的网络。它使用如下：

1. 使用 shift-click 鼠标操作精确选择两个焊盘。
2. 单击按钮。

在点击 SwapIt 按钮之后，连接到两个焊盘的空气线将被更换，第一焊盘连接到第二焊盘的网，反之亦然。

##### DumpIt 工具

此工具用于编写包含 WireIt，CutIt 和 SwapIt 工具所做更改列表的文件。这是通过将当前 PCB 网表与首次激活 WireIt 工具时存在的网表进行比较来完成的。

单击该按钮将显示一个对话框窗口，您可以在其中指定用于存储布线更改列表的文件。（您可以键入文件名，使用文件浏览器，或将文件拖放到对话框窗口中的文本字段。）单击 OK 按钮会将布线已更改的文本列表写入文件。（该文件的任何先前内容都将被覆盖。）然后，您负责手动将网表更改注释到与此 PCB 布局关联的原理图中。单击该 Cancel 按钮将中止文件的写入。

##### 一些其他操作

将焊盘从一个网移动到另一个网

1. 选择焊盘。
2. 使用 CutIt 工具断开焊盘与任何网络的连接。
3. 再次选择焊盘（在 CutIt 操作后将取消选择它们）。
4. 在要连接焊盘的网络上选择焊盘，轨道或区域。
5. 单击 WireIt 工具将焊盘连接到选定的网络。

#### [KiCad 的交互式 HTML BOM 插件](https://github.com/openscopeproject/InteractiveHtmlBom)

- 该插件生成方便的 BOM 清单，能够在视觉上关联并轻松搜索 PCB 上的元件及其放置。
- 这在手工焊接原型时非常有用，你必须找到 50 个应该有 0.1uF 电容的地方，或者哪个 SOP8 封装用于相同的微型。动态突出显示 PCB 渲染中同一组中的所有元件，可以更轻松地手动填充电路板。
- 这个插件利用 Pcbnew Python 绑定来读取 PCB 数据并渲染丝网印刷，fab 层，封装焊盘，文本和绘图。此外，如果您通过 Eeschema 可以从其内部 BOM 工具生成的网表或 xml 文件导出数据，它可以从原理图中提取数据。额外数据可以作为 BOM 表中的附加列添加（例如厂商 ID ），也可以用于指示应该完全省略哪些元件（dnp字段）。有关功能的完整描述，请参阅 [wiki](https://github.com/openscopeproject/InteractiveHtmlBom/wiki)。
- 生成的 HTML 页面是完全独立的，不需要互联网连接即可工作，可以与项目文档一起打包或托管在网络上的任何位置。
- [演示胜过千言万语](https://openscopeproject.org/InteractiveHtmlBomDemo/)

##### 安装和使用

有关说明，请参阅项目 [Wiki](https://github.com/openscopeproject/InteractiveHtmlBom/wiki)。

```bash
Windows 安装：

git clone https://github.com/openscopeproject/InteractiveHtmlBom.git %appdata%/kicad/scripting/plugins/InteractiveHtmlBom

Linux 安装：

git clone https://github.com/openscopeproject/InteractiveHtmlBom.git ~/kicad/scripting/plugins/InteractiveHtmlBom

或：

git clone https://github.com/openscopeproject/InteractiveHtmlBom.git ~/.kicad_plugins/InteractiveHtmlBom

Mac OS 安装：

git clone https://github.com/openscopeproject/InteractiveHtmlBom.git ~/Library/Application Support/kicad/scripting/plugins/InteractiveHtmlBom

新版 Mac OS：

git clone https://github.com/openscopeproject/InteractiveHtmlBom.git ~/Library/Preferences/kicad/scripting/plugins/InteractiveHtmlBom

```

##### 许可证和基本信息

插件代码在 MIT 许可下获得许可，LICENSE 有关详细信息，请参阅。

HTML 页面使用嵌入到生成的 BOM 页面中的 Split.js 和 PEP.js 库。

units.py 是从 KiBom 插件（麻省理工学院许可证）借来的。

svgpath.py 很大程度上基于 svgpathtools 模块（MIT 许可证）。



#### [KiCad 动作插件](https://github.com/MitjaNemec/Kicad_action_plugins)

此存储库包含 KiCad pcbnew Action Plugins()

所有插件都已经在 Windows7 上的 Kicad 5.1-1 上进行了测试。您需要启用 KICAD_SCRIPTING_WXPYTHON。即使使用 Python3，他们也应该使用 GNU/Linux发行版和 MacOS。

根据 Python Plugin Development for Pcbnew 指南，这些插件已经被开发为一个复杂的插件。

将 teplugin copy 相关文件夹安装到 KiCad 配置文件夹的 “scripting / plugins” 子文件夹中：

```bash
Windows 安装：

git clone https://github.com/MitjaNemec/Kicad_action_plugins.git %appdata%/kicad/scripting/plugins/Kicad_action_plugins

Linux 安装：

git clone https://github.com/MitjaNemec/Kicad_action_plugins.git ~/kicad/scripting/plugins/Kicad_action_plugins

或：

git clone https://github.com/MitjaNemec/Kicad_action_plugins.git ~/.kicad_plugins/Kicad_action_plugins

Mac OS 安装：

git clone https://github.com/MitjaNemec/Kicad_action_plugins.git ~/Library/Application Support/kicad/scripting/plugins/Kicad_action_plugins

新版 Mac OS：

git clone https://github.com/MitjaNemec/Kicad_action_plugins.git ~/Library/Preferences/kicad/scripting/plugins/Kicad_action_plugins

```

##### 复制布局

此插件的目的是复制布局部分。复制基于分层表。复制的基本要求是要复制的部分完全包含在单个分层工作表中，复制部分只是同一工作表的副本。支持复杂的层次结构，因为复制的表可以包含子表。该插件复制了封装，区域，轨道和文本。

在复制部分（枢轴部分）布局后（封装，轨道，文本对象和区域放置），您需要：

1. 放置要复制的部分的锚封装。这定义了复制部分的位置和方向。您可以使用 [放置封装] 动作插件。
2. 在枢轴部分中选择相同的锚点。
3. 运行插件。
4. 选择要复制的层级。
5. 选择要复制的工作表（默认为全部工作表）
6. 选择是否还要复制轨道，区域和/或文本对象。
7. 选择是要复制与枢轴边界框相交的轨道/区域/文本，还是仅复制包含在边界框内的轨道/区域/文本。
8. 选择是否要删除已布局的曲目/区域/文本（这在更新已复制的布局时很有用）。
9. 点击OK。

此外，您还可以选择是否还要复制区域，文本和/或轨道。默认情况下，只复制包含在由该部分中的所有封装构成的边界框中的对象。您还可以选择复制与此边界框相交的区域和轨迹。此外，可以删除已在复制的边界框中布局的轨道，文本和区域（在更新时很有用）。请注意，无论截面方向如何，边界框都是与x和y轴对齐的正方形。

复杂分层项目的复制示例。首先复制内层，然后复制外层。

复制

##### 放置封装

这个插件将封装放在：

- 排队
- 圆
- 在方阵中

用于放置的插件可以通过连续的参考编号或不同的分层表上的相同 ID 来选择。

如果您想通过连续的参考号码来放置封装

1. 选择要放置的序列中的第一个封装
2. 运行插件
3. 按参考编号选择哪个地方
4. 选择要放置的序列中的封装
5. 选择排列（线性，矩阵，圆形）
6. 选择位置尺寸（在线性和矩阵模式下的x和y轴中的步进以及在圆周模式下的角度步长和半径）
7. 运行插件

如果你想用相同的 ID 放置封装

1. 选择要放置的序列中的第一个封装
2. 运行插件
3. 选择将放置封装的层次级别（在复杂的层次结构中）
4. 选择要放置封装的工作表
5. 选择排列（线性，矩阵，圆形）
6. 选择位置尺寸（在线性和矩阵模式下的x和y轴中的步进以及在圆周模式下的角度步长和半径）
7. 运行插件
8. 按参考编号放置的示例 按参考号码排列

按工作表 ID 放置的示例 按工作表 ID 放置 

##### 删除所选

此插件删除所选项目。项目可以是：区域和/或轨道和/或封装。主要目的是删除选定的轨道以重做部分布局。

要运行插件：

1. 选择要删除的项目（请注意，如果从左侧或右侧启动选择框，则在 KiCad 中会有所不同）
2. 运行插件
3. 选择要删除的内容
4. 点击 OK

删除选定的曲目和区域

##### pad2pad 轨道距离

此插件计算两个焊盘之间的最短距离。请谨慎使用，因为算法遵循轨道布局，结果并不总是正确的。此外，不考虑过孔距离。下图显示距离不正确的示例。这里，算法计算从第一个焊盘到第4个引脚然后到另一个焊盘的距离。它没有考虑在它们实际分支的环绕区域处的两个轨道之间的连接，使得测量的距离比实际更长。 轨道布局混淆了算法

对于复杂轨道（GND，电源轨），计算可能需要相当长的时间。

要运行插件：

1. 选择两个焊盘片来测量它们之间的距离
2. 运行插件
3. 选择要删除的内容
4. 点击 OK

测量焊盘到焊盘距离

##### net2net 最小距离

此插件计算不同网络上两个轨道之间的最短距离。要使用，请在第一个网络上选择一个焊盘，在第二个网络上选择一个焊盘并运行该插件。

##### 压缩工程

这个插件压缩工程，从而使其可移植。

原理图归档是通过项目缓存库实现的。项目缓存库被复制到 project-archive.lib，它被修改并添加到项目符号库表中（如果表不存在则创建它）。此外，修改了原理图中符号的链接，以便它们指向归档库中的符号。然后，删除缓存库。Eeschema 将在下次编辑原理图时重新创建正确的缓存库。

pcb 及其封装的存档已经在 pcbnew 中实现。

3D 模型存档位于 “shapes3D” 子文件夹中，其中复制所有 3D 模型。然后，修改布局（.kicad_pcb）文件中模型的链接，以便它们指向具有相对于项目文件夹的路径的归档 3D 模型。

该插件从 pcbnew 运行。当插件运行时，必须关闭 eeschema。如果插件成功完成，它会自动关闭 pcbnew。预期此行为是执行操作所必需的。

如果项目稍后被修改，则应该再次存档以便保持可移植性。如果必须更换单元的符号，则必须删除具有相同符号的所有单元。

##### 交换引脚

这个插件交换两个焊盘（布局）和它们相应的引脚（原理图中）。原理图中的引脚必须直接或通过短线段连接到本地或全局标签或分层标签。该插件还适用于多单元元件和/或跨不同层级。

只能连接一个引脚。目前不支持“无连接”标志。当插件在 pcbnew 中执行时，必须关闭 Eeschema。插件完成后，您应该保存布局。请注意，使用undo只会撤消布局中的更改，而不会撤消原理图中的更改。要反转操作，可以再次运行插件。

引脚交换示例 在本地标签上交换引脚

##### 交换单位

这个插件交换两个单元（布局）和 r 单元（原理图）。当插件在 pcbnew 中执行时，必须关闭 Eeschema。跨分层页面的单元交换工作。请注意，使用undo只会撤消布局中的更改，而不会撤消原理图中的更改。要反转操作，可以再次运行插件。

单元交换示例

交换不同分层页面中的单位

##### 长度统计

此插件显示所选网络上所有轨道的长度。这可以用于长度匹配。

工作流程：

- 选择要显示长度的网络上的轨道或焊盘。您还可以选择封装
- 运行插件。您可以删除冗余网络
- 铺设轨道
- 关闭轨道长度窗口
例： 长度统计

##### 保存/恢复布局

此插件保存所选 pcb 的部分布局（仅保存特定的层级）。如果它基于相同的分层子表，则可以将此布局导入另一个 pcb。测试和检查相等性。

工作流程：

- 选择要为其保存布局的层次结构中的一个封装。
- 运行插件并选择“保存布局”
- 选择要保存布局的文件
- 创建一个新项目。将用于层次结构的所选.sch文件复制到项目文件夹，并将其链接到原理图
- 创建一个新布局。
- 放置并选择锚点。这定义了恢复布局的位置和方向。
- 运行插件。
- 选择“恢复布局”9：选择存储布局数据的文件
- Voila，观察恢复的布局
例： 长度统计


#### [KiCad 射频工具](https://github.com/easyw/RF-tools-KiCAD)

兼容性：KiCAD 5.1.4。

轨道的圆化工具。
(将复制到 KiCad 插件目录)。

```bash
Windows 安装：

git clone https://github.com/easyw/RF-tools-KiCAD.git %appdata%/kicad/scripting/plugins/RF-tools-KiCAD

Linux 安装：

git clone https://github.com/easyw/RF-tools-KiCAD.git ~/kicad/scripting/plugins/RF-tools-KiCAD

或：

git clone https://github.com/easyw/RF-tools-KiCAD.git ~/.kicad_plugins/RF-tools-KiCAD

Mac OS 安装：

git clone https://github.com/easyw/RF-tools-KiCAD.git ~/Library/Application Support/kicad/scripting/plugins/RF-tools-KiCAD

新版 Mac OS：

git clone https://github.com/easyw/RF-tools-KiCAD.git ~/Library/Preferences/kicad/scripting/plugins/RF-tools-KiCAD

```

- 轨迹圆化(动作插件)。
- 焊料蒙版扩展器(动作插件)。
- 轨迹长度(动作插件)。
- 通过栅栏生成器(动作插件)[需要速裁器]


#### [KiCad-动作-脚本](https://github.com/jsreynaud/kicad-action-scripts)


```bash
Windows 安装：

git clone https://github.com/jsreynaud/kicad-action-scripts.git %appdata%/kicad/scripting/plugins/kicad-action-scripts

Linux 安装：

git clone https://github.com/jsreynaud/kicad-action-scripts.git ~/kicad/scripting/plugins/kicad-action-scripts

或：

git clone https://github.com/jsreynaud/kicad-action-scripts.git ~/.kicad_plugins/kicad-action-scripts

Mac OS 安装：

git clone https://github.com/jsreynaud/kicad-action-scripts.git ~/Library/Application Support/kicad/scripting/plugins/kicad-action-scripts

新版 Mac OS：

git clone https://github.com/jsreynaud/kicad-action-scripts.git ~/Library/Preferences/kicad/scripting/plugins/kicad-action-scripts

```


#### [KiCad BGA 工具](https://github.com/Laksen/kicad-bga-tools)


```bash
Windows 安装：

git clone https://github.com/Laksen/kicad-bga-tools.git %appdata%/kicad/scripting/plugins/kicad-bga-tools

Linux 安装：

git clone https://github.com/Laksen/kicad-bga-tools.git ~/kicad/scripting/plugins/kicad-bga-tools

或：

git clone https://github.com/Laksen/kicad-bga-tools.git ~/.kicad_plugins/kicad-bga-tools

Mac OS 安装：

git clone https://github.com/Laksen/kicad-bga-tools.git ~/Library/Application Support/kicad/scripting/plugins/kicad-bga-tools

新版 Mac OS：

git clone https://github.com/Laksen/kicad-bga-tools.git ~/Library/Preferences/kicad/scripting/plugins/kicad-bga-tools

```


#### [KiCad-diff](https://github.com/Gasman2014/KiCad-Diff)

我在 MacOS10.12 上运行这个安装程序，但是我想任何 Linux 变体都可以工作。Windows-YMMV.

##### 相依性。
* gsed(Mac sed 有限)。
* 启用 Python 脚本的 KiCad。
* 形象魔术。
* Fossil SCM(或 Git 或 SVN)。
* 可能还有其他一些，但所有都是在 BREW 的帮助下安装的。
(如果您使用的是 MacOS X，并且 Imagemagick 的转换有问题，您可以尝试使用 RSVG 库重新安装它。使用自制软件：
BREW 删除 Imagemagick。
BREW 安装 imagemagick--with-library svg。

** 使用说明 **

* 检查您的配置管理工具的路径是否正确(第32-34行)。
* 在 /usr/local/bin 中安装 plotPCB2.py (或调整第480/481行的路径)。
(我可能应该将这类内容添加到配置文件中，但我可能会等待V2.)。
* 安装 imagemagick。
* 运行主脚本，并从 GUI 中选择源代码管理存储库中的一对版本。
* 选择分辨率。
* 选择要比较的图层。
该脚本应该构建一系列 SVG 文件，并在网页中显示差异。

##### 计划：
用 Python 重写以改进与 KiCad 的集成。
干完。
可能支持其他 VCS 工具。
干完。
选择图层集和分辨率的机制。
干完。
改进了文本差异的解析和意义。
进行中



#### [KiCad 泪滴脚本](https://github.com/NilujePerchut/kicad_scripts)

kicad_script

一些 kicad 脚本

泪滴

此操作插件向 PCB 添加和删除泪滴。
有关它的详细信息，请参阅 teardrop 目录中的自述文件。

#### [KiCad 配色脚本](https://github.com/skalidindi3/kicad-colors)

KiCad 配色

快速交换 KiCad 主题/配色方案。
KiCad 的默认颜色似乎是从 R、G 和 B 组件的每个值的值 [0、132、194、255]中选择的。
我们可以修改这些颜色以获得更美观的效果。
创意&来自 https://github.com/pointhi/kicad-color-schemes 的启示

用法：
调用生成文件将就地修改您当前的配置文件

```bash
git clone https://github.com/skalidindi3/kicad-colors.git 

macOS: ~/Library/Preferences/kicad/eeschema
Linux: ~/.config/kicad/eeschema

```

创建备份

```bash
# 列出 eesschema 的可用主题
$ make show_eeschema_options
set_eeschema_base16_dracula
set_eeschema_base16_nord
set_eeschema_base16_oceanicnext
set_eeschema_base16_onedark
set_eeschema_base16_rebecca
set_eeschema_default
set_eeschema_handpicked_nord
set_eeschema_handpicked_onedark

# 为 eesschema 选择特定主题
$ make set_eeschema_base16_nord

# 切换回原始 eesschema 颜色
$ make set_eeschema_default
```


#### [KiCad 配色主题](https://github.com/pointhi/kicad-color-schemes)


如何使用色彩主题
每个主题目录都包含在您的个人配置文件中找到的 eesschema 和 pcbnew 设置文件的颜色定义部分。

```bash
git clone  https://github.com/pointhi/kicad-color-schemes.git
Linux 下： ~.config/kicad/
Windows XP: “C:\Documents and Settings\username\Application Data” + kicad (= %APPDATA%\kicad)
Windows Vista 或更高： “C:\Users\username\AppData\Roaming” + kicad (= %APPDATA%\kicad)
OSX: 用户 home 目录 + /Library/Preferences/kicad
```

使用文本编辑器使用在此文件夹的文件中找到的数据覆盖相关部分。
请确保先创建备份。
pcbnew 配置文件内容已被分成负责封装外形编辑器的部分和负责 pcbnew 的部分。
这样做是为了让您可以更轻松地混合和匹配不同工具的不同方案。

自动打补丁程序。
自动补丁脚本可用于将配色方案传输到 KiCad 设置文件中。
在使用 KiCad 之前，请确保它已关闭。
脚本期望包含配色方案的目录和 KiCad 配置目录作为参数。
包括开关以禁用方案定义的特定部分的传输。
(有关详细说明，请使用 --help。)。
在进行更改之前，将创建设置文件的备份。

```bash
python3 patch.py ~/kicad-color-schemes/blue-green-dark/ ~/.config/kicad/
```

#### [GerberTools](https://github.com/ThisIsNotRocketScience/GerberTools)

用于加载/编辑/创建/拼板/预渲染 Gerber 文件集的工具。

感谢 [laserjiang121](https://space.bilibili.com/96274897) UP 主提供的 [KiCad原创中文视频教学98单元---插件和多 PCB 拼板软件](https://www.bilibili.com/video/BV1k64y1T7Uh/) 教程。


#### [uConfig](https://github.com/Robotips/uConfig)

##### 一个旧的个人项目重新启动，从 PDF 数据表中提取引脚并创建 KiCad 原理图。

直接保存 KiCad库文件。使用大量引脚可以更好地工作。

##### PDF 提取，它是怎么工作的？

从 `PDF` 文件中提取引脚映射是通过解析数据表来完成的。弹出器用于提取文本块，并通过魔术规则对引脚编号和引脚标签进行排序。
标签和编号通过最相关的对进行关联，以创建引脚。然后，引脚列表也按包进行排序和关联。此包列表可以另存为 `KiCad` 库。

该工具的第二部分被命名为引脚规则，以允许遵循一组名为 `KSS(Kicad Style Sheet)` 的规则进行重组。`KSS` 文件类似于 `CSS` 文件，请看一下 `KSS` 引用变量。
它存在一个命令行工具，用于从数据表中提取组件：

``` bash
uconfig datasheet.pdf -o lib1.lib -r microchip.kss
```

它将以 `microchip.kss` `KSS` 文件规则格式保存在 `dataSheet.pdf` 中的所有元件原理图中，保存在 `lib1.lib` 中。还可以使用 `GUI` 界面，称为 `uconfig_gui`。

##### KSS，KiCad 样式表。

以一种创新的方式为受 `CSS` 启发的原理图元件定义主题。这可以与 `kicad Library Linter` 相提并论。您可以在 `rules/ directory` 中找到示例，也可以查看`KSS` [引用变量](https://github.com/Robotips/uConfig/blob/master/rules/README.md)。

##### 二进制文件下载

为方便起见，以下网址提供了预建项目：

```bash
https://ci.appveyor.com/api/projects/sebcaux/uconfig/artifacts/uconfig-win32-v0.zip
```


## [KiCad 中国群文件](./KiCad_CN_group_files.md)
