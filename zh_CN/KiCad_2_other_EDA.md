@[toc]

## [KiCad 中国群文件](./KiCad_CN_group_files.md)

## [KiCad 与其他 EDA 转换](./KiCad_2_other_EDA.md)

## 本文档由 Heyden 编写

## 起因

随着从其他 EDA 转KiCad 的越来越多，为了使用方便，转换来自其他软件的 PCB 文件，在这里我总结了一下方法。其他软件如 OrCAD、PADS 、Cadence Allegro 等都可以通过 AD 转换成 KiCad 格式！把其他软件的 PCB 文件通过 Altium Designer 的输入向导导入 AD,然后根据下面方法转换成 KiCad 格式。
立创 EDA 可以直接导出 AD 格式！

1. 用 AD 打开导出 `P-CAD` 格式，使用 [pcbnew( 独立版)][1] 导入。

2. 使用 lc2kicad 转换。此工具是群里 [RigoLigo](https://gitee.com/rigoligo) 开发，是命令行工具，[群文件仓库](./KiCad_CN_group_files.md)和群里有 exe 可执行文件，立创导出的 `JSON` 格式文件拖动到LC2KICAD工具即可转换。不支持原理图转换。

3. 使用 Atok 工具转换，本工具由 KiCad 论坛 60 多岁的老爷子基于 altium2Kicad开发的，使用 C# 开发，带有用户使用界面，比较友好，可以把转换的 PCB 导出并生成 KiCad 封装库，可以提取 AD 封装的 3D 模型，还自动关联封装。只是目前还不支持原理图转换。此工具我已经上传到群文件。

4. 使用 altium2kicad 工具转换，原理图和 PCB 都可以。使用 Perl 脚本开发，需要安装 Perl支持软件才能使用，不带界面。但可以转换 AD 的原理图和 PCB 文件。群文件找此工具。

5. 使用最新版本的 kiCad 5.99 转换，原理图和 PCB 都可以。

 [1]: PCBNEW 独立版，Linux 终端方式：Linux 可以直接在终端中运行 `pcbnew` 即可打开；Linux 通过桌面方式：在应用程序里面搜索 `pcbnew` 会显示一个 `PCBNEW (独立版)` 的标识。mac OS 同理。Windows 通过桌面方式：在开始菜单找到 `KiCad 应用程序的分类文件夹` 单击 `PCBNEW` 即可运行独立版 PCBNEW。

## [kicad-allegro](https://github.com/system76/kicad-allegro)（基于 Rust 的 Allegro 提取 (ASCII) 到 KiCad 转换器和查看器）

1. 编译 kicad-allegro

- [Arch Linux 的 Rust 编译和打包规则](https://wiki.archlinux.org/title/Rust_package_guidelines)
- 其他 Linux 发行版可以参考
- [在 Windows 上通过 Rust 进行开发](https://docs.microsoft.com/zh-cn/windows/dev-environment/rust/)
- [Rust 交叉编译 Mac 编译 Linux 平台](https://www.qttc.net/529-rust-cross-compile-mac-to-linux.html)
- [crm (Cargo registry manager)](https://github.com/wtklbm/crm) 是一个在终端运行的镜像管理程序，能够对 Cargo 镜像源进行简单的添加、修改、删除操作，并能帮助您快速的切换不同的 Cargo 镜像源。crm 内置了多种国内 (中国) 镜像源，它们分别是：sjtu, tuna, ustc, rsproxy，bfsu, nju, hit。

2. 打包 kicad-allegro

- [Arch Linux 的 Rust 编译和打包规则](https://wiki.archlinux.org/title/Rust_package_guidelines)
- 其他 Linux 发行版可以参考
- [在 Windows 上通过 Rust 进行开发](https://docs.microsoft.com/zh-cn/windows/dev-environment/rust/)
- [Rust 交叉编译 Mac 编译 Linux 平台](https://www.qttc.net/529-rust-cross-compile-mac-to-linux.html)
- [crm (Cargo registry manager)](https://github.com/wtklbm/crm) 是一个在终端运行的镜像管理程序，能够对 Cargo 镜像源进行简单的添加、修改、删除操作，并能帮助您快速的切换不同的 Cargo 镜像源。crm 内置了多种国内 (中国) 镜像源，它们分别是：sjtu, tuna, ustc, rsproxy，bfsu, nju, hit。

3. 安装 kicad-allegro

- Arch 系用户可以通过 AUR 包安装: [kicad-allegro](https://aur.archlinux.org/packages/kicad-allegro) 或 [kicad-allegro-git](https://aur.archlinux.org/packages/kicad-allegro-git)

## [LCKiCad Converter](http://blog.xtoolbox.org/lceda_kicad_lib/) 一个将 lceda.cn 的器件转换为 KiCad 格式的浏览器扩展. 可以转换原理图库，封装库以及 3D 模型

1. 浏览器在线安装 LCKiCad

- 最新版zip包（快于商店），解压后在开发者模式下安装 [http://blog.xtoolbox.org/lckiconverter2.0.zip](http://blog.xtoolbox.org/lckiconverter1.3.zip)

- Google商店地址 LCKiConverter – [Chrome 网上应用店 (google.com)](https://chrome.google.com/webstore/detail/lckiconverter/lbgkkidccknjbofkefinfempaamjcmhb)

- Edge商店地址 LCKiConverter – [Microsoft Edge Addons](https://microsoftedge.microsoft.com/addons/detail/lckiconverter/fmebjbgbgkgpogefaogfpdmfemlpnpaa)

- 代码托管地址 [https://www.github.com/xtoolbox/lckiconverter](https://www.github.com/xtoolbox/lckiconverter)

2. 浏览器中使用 LCKiCad

- 在浏览器中安装插件，并在 [LCEDA 标准版](https://lceda.cn/editor) 或 [LCEDA 专业版](https://pro.lceda.cn/editor) 中允许使用。进入在线版的 LCEDA，当看到<kbd>帮助</kbd>菜单旁边出现 <kbd>KiCad</kbd> 标志时，说明插件运行成功。

- 在 [EasyEDA 标准版](https://easyeda.com/editor) 或 [EasyEDA 专业版](https://pro.easyeda.com/editor) 用使用方式与 [LCEDA 标准版](https://lceda.cn/editor) 或 [LCEDA 专业版](https://pro.lceda.cn/editor) 相同。

3. 在 LCEDA 标准版中使用 LCKiCad

- 下图分别为 LCEDA 标准版中插件启动成功后，菜单栏样式。

![LCEDA 标准版](../images/lceda_kicad_lib/lcstd_logo.png)

- 在标准版中，通过<kbd>放置</kbd> -> <kbd>符号</kbd>菜单打开元件库列表。同时使用 <kbd>KiCad</kbd> 图标开打转换插件的界面。

- 选择要想下载的内容，然后点击<kbd>下载</kbd>按钮下载。当<kbd>3D 模型</kbd>按钮为绿色时，可以预览 3D 模型。

![在 LCEDA 标准版中使用 LCKiCad](../images/lceda_kicad_lib/lcstd_select.png)

3. 在 LCEDA 专业版中使用 LCKiCad

- 下图分别为 LCEDA 专业版中插件启动成功后，菜单栏样式。

![LCEDA 专业版](../images/lceda_kicad_lib/lcpro_logo.png)

- 在专业版中，通过正文元件库列表来选择想要的内容，保持转换插件的界面是打开状态。

- 选择要想下载的内容，然后点击<kbd>下载</kbd>按钮下载。当<kbd>3D 模型</kbd>按钮为绿色时，可以预览 3D 模型。

![在 LCEDA 专业版中使用 LCKiCad](../images/lceda_kicad_lib/lcpro_select.png)

4. LCKiCad 使用注意事项

- 下载内容为一个压缩包，其中包含 KiCad 格式的原理图库、封装库以及 3D 模型。所有库文件的名称都以插件设置中的 `Output file Prefix` 为前缀。

- 库的路径会通过压缩包的配置文件自动加入到工程中，因此把压缩包解压到 KiCad 工程中，再次打开工程就能使用下载的库。
