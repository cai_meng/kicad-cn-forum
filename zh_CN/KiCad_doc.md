@[toc]

## [KiCad 安装](./KiCad_install.md)

## [KiCad 文档](./KiCad_doc.md)

### KiCad 在线文档

[KiCad 在线简体中文文档](http://docs.kicad-pcb.org/master/zh/) 目前简体中文文档和 i18n 由 [kicad-cn](https://github.com/kicad-cn) 和 [taotieren](https://github.com/taotieren) 提供翻译和维护。

### KiCad 离线文档

- [KiCad 文档简体中文](https://gitee.com/KiCAD-CN/KiCad-doc_build)仓库，由于 Gitee 仓库大小限制目前只提供编译完后英文原版和简体中文文档下载。
- [KiCad 文档简体中文 PDF 下载](https://gitee.com/KiCAD-CN/KiCad-doc_build/releases) 发行版只提供 PDF 文档，需要其他格式文档请自行到仓库对应目录下载。
- 因为 KiCad 在线文档的 CI/CD 服务所使用的是 Ubuntu 14.04 系统，和我本地使用的 Manjaro Linux 编译系统不一致，会存在简体中文文档编译后显示不全，甚至部分表格排序错误等问题，目前正在和 KiCad 文档团队沟通此问题，暂时提出使用多种不同编译环境来编译文档，最后发行版本使用哪个编译环境还没确定。也有提出更新文档编译系统，也有提出更新文档编译方式，因为编译的文档是通过 latex 来进行的。目前的 latex 是使用 Python2 开发，现在 Python 官方将放弃 Python2 的支持。
- 如果需要阅读简体中文文档前使用离线文档阅读或校对，如果在文档阅读中遇到不明白或者更好建议都可以到仓库下提交建议或者问题。
- 每次的简体中文文档发行版都会提供相应的修复和更新说明。


### 配置 git 代理

#### 配置 git 的 http https 代理

Linux 和 Windows 都适用

``` bash
# gitlab 服务器在国外下载速度速度收到很大影响。下面对 gitlab 配置 http https 代理。同理也可以对 github 配置 http https 代理。
git config --global http.https://gitlab.com.proxy socks5://127.0.0.1:1080
git config --global http.https://github.com.proxy socks5://127.0.0.1:1080

# 其中 socks5://127.0.0.1:1080 换成你使用代理服务。如：
git config --global http.https://gitlab.com.proxy http://127.0.0.1:8080

```

####  配置 git 的 ssh 代理

Linux 系统

``` bash
# 需要安装 openbsd-netcat 来实现转发，以 Manjaro Linux 安装为例：
sudo pacman -S openbsd-netcat
```

```bash
# 在用户目录下的 .ssh/ 创建 config 文件
vim ~/.ssh/config
# 详细配置如下
```

``` linux_ssh_config
Host github.com
Host gitlab.com
ProxyCommand nc -X 5 -x 127.0.0.1:1080 %h %p
HostName %h
Port 22
User git
IdentityFile  ~/.ssh/id_rsa
IdentitiesOnly yes
```

Windows 系统

Windows 10 带有 connect 转发工具无需手动安装。同样也是在 在用户目录下(`c:\User\username\.ssh\`)的 `.ssh\` 创建 `config` 文件

``` windows_ssh_config
Host github.com
Host gitlab.com
ProxyCommand connect -S 127.0.0.1:1080 %h %p # -H 为 HTTP
HostName %h
Port 22
User git
IdentityFile  ~/.ssh/id_rsa
IdentitiesOnly yes
```

### KiCad  i18n 翻译

[KiCad i18n](https://gitlab.com/kicad/code/kicad-i18n) 官方仓库。翻译使用的工具为 [PoEdit](https://poedit.net/) 不同操作系统下载安装不同版本的工具。

#### 克隆 kicad 源代码和 kicad-i18n 仓库

``` bash
# 由于我在 kicad-i18n/zh_CN/kicad.po 中配置了自动从源代码更新翻译的功能。
# 目录名为 kicad-source-mirror 。克隆下来的源码仓库请重命名成 kicad-source-mirror 
# 需要将 kicad-source-mirror 和 kicad-18n 放在同一级目录下。
git clone https://gitlab.com/kicad/code/kicad.git kicad-source-mirror

# 克隆 kicad-i18n 翻译仓库
git clone https://gitlab.com/kicad/code/kicad-i18n.git
```

#### 切换分支
克隆下来的仓库默认是在 `master` 分支下。如果如果修改其他分支翻译需要切换两个仓库的分支状态。

KiCad 源码仓库分支切换
``` bash
# 进入 kicad 源码目录
cd kicad-source-mirror
# 切换到 5.1 分支
git checkout 5.1

# 切换到 master 分支
git checkout master
```

kicad-i18n 仓库分支切换
``` bash
# 进入 kicad 源码目录
cd kicad-i18n
# 切换到 5.1 分支
git checkout 5.1

# 切换到 master 分支
git checkout master
```

#### 中文文案排版指南（翻译前前先阅读，注重排版规则，给用户带来更好的阅读体验。）

[中文文案排版指南](https://github.com/mzlogin/chinese-copywriting-guidelines) 仓库 ，
镜像地址：[https://gitee.com/KiCAD-CN/chinese-copywriting-guidelines](https://gitee.com/KiCAD-CN/chinese-copywriting-guidelines)
在线阅读地址：[https://mazhuang.org/wiki/chinese-copywriting-guidelines/](https://mazhuang.org/wiki/chinese-copywriting-guidelines/)

#### 关于 PoEdit 设置

补充一点关于 PoEdit 从源代码中更新的操作。
按照我上述的 `git clone` 操作的一步步进行的话。单击 PoEdit 中的 `从源代码更新` 就能自动从源代码中获取最新的翻译文件。

关于 PoEdit 的翻译操作可以参考 KiCad-doc 中的 《[如何使用 GUI 翻译](https://gitee.com/KiCAD-CN/KiCad-doc_build/tree/master/src/gui_translation_howto/zh)》手册。

翻译后单击 `保存` 按钮会进行自动编辑检测会自动在 `zh_CN` 目录下生成 `kicad.mo` 文件。可以用于下面的测试操作。
根据给出的提示就行操作。不懂的仓库提问。

#### kicad-i18n 翻译文件测试

Linux 下 `master` 分支翻译文件测试。以 Manjaro Linux （Arch Linux 也可以）为例：
配置 `ArchLinuxCN` 源使用方法：在 `/etc/pacman.conf` 文件末尾添加以下两行：

``` bash
[archlinuxcn]
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
```

之后安装 `archlinuxcn-keyring` 包导入 GPG key。

安装 KiCad master 分支版本：

``` bash
# 打包的时候将 kicad-git 和 kicad-i18n-git 合并成 kicad-git。只需安装 kicad-git 即可
yay -S kicad-git
```

替换翻译文件

``` bash
# 先备份旧的翻译文件。以防新的翻译文件存在严重问题导致 KiCad 运行崩溃。无法进行测试。
sudo mv /usr/share/kicad/internat/zh_CN/kicad.mo /usr/share/kicad/internat/zh_CN/kicad.mo.bak

# 复制 zh_CN/kicad.mo 到 /usr/share/kicad/internat/zh_CN/kicad.mo
sudo cp zh_CN/kicad.mo /usr/share/kicad/internat/zh_CN
```

启动 KiCad 就能进行对应测试的操作。看看翻译有没有什么严重问题。显示问题排版问题等等。

Windows 到[国内镜像站](./KiCad_install.md) 获取 `夜间测试版本` 下载安装。
在 **KiCad 安装目录** 下 `[KiCad 安装磁盘]:/Program Files/KiCad/share/kicad/internat/zh_CN/kicad.mo`  先备份原始的翻译文件。然后再用新的文件进行替换。
同样也能下载    `5.1` 分支的测试版本进行翻译测试。

#### 提交 KiCad 翻译到官方仓库

[kicad-i18n](https://gitlab.com/kicad/code/kicad-i18n) 官方仓库，需要注册 GitLab 账户。
这里就用通过网页的方式提交合并请求（PR）：
在 kicad-i18n 网页仓库上 `master` 这里选择你想提交的你本地对应翻译分支。
如你本地完成 `5.1` 分支的翻译就在网页仓库这里选择 `5.1` 分支，然后单击 `zh_CN/kicad.po` 进去后单击 `编辑` 按钮；
会出现一个在线的编辑框。用文本编辑器打开你本地对应分支的 `kicad.po` 文件并复制里面的内容，全选在线编辑框的内容并删除，然后粘贴你本地的翻译内容到在线编辑框中。
提交信息建议填写保持仓库的简洁性：Update Simplified Chinese translation
然后单击 `Commit changes`（提交变更） 完成变更会进入到 `New Merger Repuest`(新建合并请求)，还只是完成了变更过程，还需要一步提交过程。
如果你需要写一些描述信息就可以在 `Description`（描述）中书写你想表达的内容。填写完成后单击 `Submit 合并请求` 按钮。完成合并请求提交。

如果想通过 `git` 命令完成合并请求提交，这个参考[精通 Git 手册第二版](https://git-scm.com/book/zh/v2)中介绍的 Git 命令行操作。

## KiCad 开发者手册

在线简体中文版 Web 版本：[https://dev-docs.kicad.org/zh-cn/](https://dev-docs.kicad.org/zh-cn/)

KiCad 官方仓库地址：[https://gitlab.com/kicad/services/kicad-dev-docs](https://gitlab.com/kicad/services/kicad-dev-docs)

简体中文翻译分支：[https://gitlab.com/taotieren/kicad-dev-docs](https://gitlab.com/taotieren/kicad-dev-docs)

对英文版手册进行翻译整理。暂时因为 hugo 不支持 pdf 导出，其他方式导出 pdf 坑太多了。需要自行安装相关环境编译运行即可查看。

暂时使用浏览器打印 pdf 的方式制作了一份 pdf 版本。可到群仓库文件[下载](https://git.code.tencent.com/taotieren/KiCad_CN/blob/master/KiCad%20%E5%BC%80%E5%8F%91%E8%80%85%E6%96%87%E6%A1%A3/KiCad_%E5%BC%80%E5%8F%91%E8%80%85%E6%96%87%E6%A1%A3.pdf)。(注意：下载连接跳转到腾讯工蜂，可能需要登录腾讯系的帐号。)

## [KiCad 插件](./KiCad_plugin.md)
