@[toc]

## [KiCad 简介](./KiCad_description.md)

## [KiCad 安装](./KiCad_install.md)

### winget 选择安装不同版本 KiCad

1. 下载并安装 winget

[winget](https://github.com/microsoft/winget-cli/releases) 发行版仓库。选择适合的发行版下载。（仅限 Windows 1709 以后的 Windows 发行版）

2. 更新 winget 源

```bash
winget.exe source update
```
3. 通过 winget 搜索来获取 KiCad 软件包信息

```bash
winget.exe search kicad
winget.exe search eda
winget.exe search pcb
winget.exe search cad
```

4. 通过 winget 安装 KiCad

```
winget.exe install kicad
```
5. 欢迎大家到 [winget-pkgs](https://github.com/microsoft/winget-pkgs) 仓库贡献软件包

打包操作可以参考我提交的 [KiCad 5.1.6 的 PR](https://github.com/microsoft/winget-pkgs/pull/421)

```bash
https://github.com/microsoft/winget-pkgs/pull/421
```

6. KiCad 的 winget 打包的 `yaml` 文件内容。现在贴出来方便大家学习。

```yaml
Id: KiCad.KiCad
Name: KiCad EDA
AppMoniker: KiCad
Version: 5.1.6
Publisher: KiCad
Author: KiCad
License: GNU AFFERO GENERAL PUBLIC LICENSE Version 3
LicenseUrl: https://gitlab.com/kicad/code/kicad/-/blob/master/LICENSE.AGPLv3
MinOSVersion: 10.0.0.0
Homepage: https://www.kicad-pcb.org
Description: A Cross Platform and Open Source Electronics Design Automation Suite
Tags: "kicad,cad,pcb,eda"
InstallerType: exe
Installers: 
  - Arch: x64 
    Url: https://kicad-downloads.s3.cern.ch/windows/stable/kicad-5.1.6_1-x86_64.exe
    Sha256: 831D99141CCBBD491F765C08A7EAFB6A385C4BBD306026ECA2B4402ED8325BD2
    Switches: 
      Silent: /S
      SilentWithProgress: /S
    

```

7. 如果使用官方的下载地址下载速度收到影响的话，可以配合国内镜像地址来获得优质得下载体验。

7.1 例如使用 `TUNA` 镜像站下载 KiCad。其他地区用户使用 `TUNA` 下载慢或者无法访问，可以参考下面收录得国内 KiCad 镜像并将修改的 `yaml` 文件另存为 `5.1.6_TUNA.yaml`：

```yaml
Id: KiCad.KiCad
Name: KiCad EDA
AppMoniker: KiCad
Version: 5.1.6
Publisher: KiCad
Author: KiCad
License: GNU AFFERO GENERAL PUBLIC LICENSE Version 3
LicenseUrl: https://gitlab.com/kicad/code/kicad/-/blob/master/LICENSE.AGPLv3
MinOSVersion: 10.0.0.0
Homepage: https://www.kicad-pcb.org
Description: A Cross Platform and Open Source Electronics Design Automation Suite
Tags: "kicad,cad,pcb,eda"
InstallerType: exe
Installers: 
  - Arch: x64 
    Url: https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/stable/kicad-5.1.6_1-x86_64.exe
    Sha256: 831D99141CCBBD491F765C08A7EAFB6A385C4BBD306026ECA2B4402ED8325BD2
    Switches: 
      Silent: /S
      SilentWithProgress: /S
    

```

7.2 使用 winget 验证 `5.1.6_TUNA.yaml` 是否有语法错误或者其他问题：

```bash
winget.exe validate 5.1.6_TUNA.yaml 
```

7.3 使用 winget 安装 KiCad，用我们修改后的 `5.1.6_TUNA.yaml` ：

```bash
winget.exe install -m 5.1.6_TUNA.yaml 
```

### KiCad 下载

#### KiCad 国内镜像

1. [清华大学 TUNA 协会](https://mirrors.tuna.tsinghua.edu.cn/kicad/) 短链接：http://tuna.kicad.cn  (推荐👍)
2. [莞工 GNU/Linux 协会](https://mirrors.dgut.edu.cn/kicad/) 短链接：http://dglinux.kicad.cn  (开源软件镜像站)
3. [南京大学开源镜像站](https://mirrors.nju.edu.cn/kicad/) 短链接：http://nju.kicad.cn （电信路线速度慢)
4. [重庆大学开源镜像站](https://mirrors.cqu.edu.cn/kicad/) 短链接：暂无
5. [哈尔滨工业大学开源镜像站](http://run.hit.edu.cn/kicad) 短链接：暂无 (仅支持 IPv6)
6. [北京外国语大学开源软件镜像站](https://mirrors.bfsu.edu.cn/kicad/) 短链接：暂无
7. [开源软件镜像站](https://mirrors.cnnic.cn/) 短链接：暂无
8. [阿里云镜像站](https://mirrors.aliyun.com/kicad/) 短链接：暂无，[详情](https://developer.aliyun.com/mirror/kicad)

#### KiCad 国外镜像：
1. [CERN 镜像](https://kicad-downloads.s3.cern.ch/index.html) （因为某些原因有时会出现无法访问）
2. [Github Releases](https://github.com/KiCad/kicad-winbuilder/releases) （因为某些原因有时会出现无法访问）


### KiCad 镜像目录使用说明（此处使用 TUNA 镜像为例说明，其余镜像同样适用）

#### [KiCad 镜像目录说明](https://mirrors.tuna.tsinghua.edu.cn/kicad/)

目录	        |	说明    |    备注
-----------|----------|---------
appimage/    |appimage 安装包|chmod +x \*.AppImage && ./*.AppImage
archive/    |压缩文件| 旧文件
docs/    |发行版的文档手册|包含 HTML、PDF、EPUB
doxygen/    |KiCad 开发配置|详细开发手册
doxygen-python/    |KiCad Python 开发配置|详细开发手册
libraries/    |封装库和集成库|发行版本的封装库和集成库
osx/    | KiCad OSX 版本目录    | 包含稳定版、夜间版和测试版
windows/    | KiCad Windows 版本目录    | 包含稳定版、夜间版和测试版
cleanup.sh    | 清理脚本    |无
favicon.ico    |图标    |无
list.js |JS 脚本    |无


#### [Windows 下载安装](http://kicad-pcb.org/download/windows/)

[KiCad Windows 版本下载](https://mirrors.tuna.tsinghua.edu.cn/kicad/windows/)

目录	        |	说明    |    备注
-----------|----------|---------
nightly/	| 夜间构建版本|无
stable/		| 发行版本|看版本号选择合适的下载
testing/	| 测试版本|选择对应分支下载测试

#### [OS X 下载安装](http://kicad-pcb.org/download/osx/)

[OS X 版本下载](https://mirrors.tuna.tsinghua.edu.cn/kicad/osx/)

目录	        |	说明    |    备注
-----------|----------|---------
nightly/	| 夜间构建版本|无
stable/		| 发行版本|看版本号选择合适的下载
testing/	| 测试版本|选择对应分支下载测试

#### Linux 下载安装

##### [AUR 包](http://kicad-pcb.org/download/arch-linux/)

添加 [ArchlinuxCN](https://mirrors.tuna.tsinghua.edu.cn/help/archlinuxcn/) 镜像:

```
yaourt -Syu
yaourt -S kicad-git
```

ArchlinuxCN 上的 kicad-git 实际上包含 kicad-git.git 和 kicad-i18n.git。kicad-i18n 能获得最新的翻译文件。

##### [Ubuntu](http://kicad-pcb.org/download/ubuntu/)

```
sudo add-apt-repository --yes ppa:js-reynaud/kicad-5.1
sudo apt update
sudo apt install --install-suggests kicad
sudo apt install kicad
```

##### [Debian](http://kicad-pcb.org/download/debian/)

```
sudo apt update
sudo apt install kicad
apt search kicad-doc
# or alternatively if you are on wheezy
apt-cache search kicad-doc
```

Debian 编译源码：

```
sudo apt install cmake doxygen libboost-context-dev libboost-dev \
libboost-system-dev libboost-test-dev libcairo2-dev libcurl4-openssl-dev \
libgl1-mesa-dev libglew-dev libglm-dev libngspice-dev liboce-foundation-dev \
liboce-ocaf-dev libssl-dev libwxbase3.0-dev libwxgtk3.0-dev python-dev \
python-wxgtk3.0-dev swig wx-common
```

##### [Linux Mint](http://kicad-pcb.org/download/linux-mint/)

```
sudo add-apt-repository --yes ppa:js-reynaud/kicad-5.1
sudo apt update
sudo apt install --install-suggests kicad
sudo apt install kicad
```

##### [Fedora](http://kicad-pcb.org/download/fedora/)

安装发行版：

```
dnf install kicad
dnf --enablerepo=updates-testing install kicad
dnf install kicad-packages3d
```

安装夜间版：

```
dnf copr enable @kicad/kicad
dnf install kicad
dnf install kicad-packages3d
dnf copr remove mangelajo/kicad
dnf install dnf-plugins-core
```

##### [OpenSUSE](http://kicad-pcb.org/download/open-suse/)

##### [Flatpak](http://kicad-pcb.org/download/flatpak/)

```
flatpak install --from https://flathub.org/repo/appstream/org.kicad_pcb.KiCad.flatpakref
```

##### [GNU Guix](http://kicad-pcb.org/download/gnu-guix/)

```
guix package -i kicad
```

##### [Gentoo](http://kicad-pcb.org/download/gentoo/)

```
emerge sci-electronics/kicad
```

##### [Sabayon](http://kicad-pcb.org/download/sabayon/)

```
equo install kicad
```

###### [源码安装](http://kicad-pcb.org/download/source/)


## Deepin/UOS 安装

### 通过 Deepin/UOS 应用商店安装

- 在应用商店搜索 `KiCad` 点击安装
- 会提示 `安装失败`，是因为缺少一些依赖包和 `KiCad` 的封装库文件 
- 在通过终端执行 `sudo apt install kicad` 如下所示：

```bash
taotieren@taotieren-PC:~$ sudo apt install kicad
正在读取软件包列表... 完成
正在分析软件包的依赖关系树
正在读取状态信息... 完成
kicad 已经是最新版 (5.0.2+dfsg1-1)。
kicad 已设置为手动安装。
升级了 0 个软件包，新安装了 0 个软件包，要卸载 0 个软件包，有 0 个软件包未被升级。
有 18 个软件包没有被完全安装或卸载。
解压缩后会消耗 0 B 的额外空间。
您希望继续执行吗？ [Y/n]
正在设置 liboce-foundation11:amd64 (0.18.2-3) ...
正在设置 kicad-footprints (5.0.2-1) ...
正在设置 xsltproc (1.1.32.1-1+deepin) ...
正在设置 kicad-demos (5.0.2+dfsg1-1) ...
正在设置 libngspice0:amd64 (30.2-1) ...
正在设置 libgl2ps1.4 (1.4.0+dfsg1-2) ...
正在设置 kicad-templates (5.0.2-1) ...
正在设置 kicad-symbols (5.0.2-1) ...
正在设置 kicad-libraries (5.0.2+dfsg1-1) ...
正在设置 libglew2.1:amd64 (2.1.0-4) ...
正在设置 liboce-modeling11:amd64 (0.18.2-3) ...
正在设置 libcurl4:amd64 (7.64.0-4+deb10u1) ...
正在设置 libwxbase3.0-0v5:amd64 (3.0.4+dfsg-8) ...
正在设置 liboce-ocaf-lite11:amd64 (0.18.2-3) ...
正在设置 libwxgtk3.0-0v5:amd64 (3.0.4+dfsg-8) ...
正在设置 liboce-visualization11:amd64 (0.18.2-3) ...
正在设置 liboce-ocaf11:amd64 (0.18.2-3) ...
正在设置 kicad (5.0.2+dfsg1-1) ...
正在处理用于 desktop-file-utils (0.23-4) 的触发器 ...
正在处理用于 mime-support (3.62) 的触发器 ...
正在处理用于 hicolor-icon-theme (0.17-2) 的触发器 ...
正在处理用于 lastore-daemon (5.0.6-1) 的触发器 ...
正在处理用于 libc-bin (2.28.7-1+deepin) 的触发器 ...
正在处理用于 man-db (2.8.5-2) 的触发器 ...
正在处理用于 shared-mime-info (1.10-1) 的触发器 ...
正在处理用于 bamfdaemon (0.5.4-1) 的触发器 ...
Rebuilding /usr/share/applications/bamf-2.index...
```

- 运行 `KiCad` 就能正常启动。

### Deepin/UOS 可以通过设置 `Debian` 的 `backports` 软件源来获取最新版本 `KiCad`

- 在 `/etc/apt/sources.list.d/` 新建 `buster-backports.list` 文件。

```bash
sudo vim /etc/apt/sources.list.d/buster-backports.list
```
- 添加如下内容，保存并退出。可以使用参考 `TUNA` 的其他 [Debian 源](https://mirrors.tuna.tsinghua.edu.cn/help/debian/) 设置

```bash
# /etc/apt/sources.list.d/buster-backports.list
deb https://mirrors.tuna.tsinghua.edu.cn/debian/ buster-backports main contrib non-free

```

- 更新软件源信息

```bash
sudo apt update
```

- 搜索 `Debian` `backports` 源中的 `KiCad`

```bash
# 在 Debian backports 中搜索 KiCad
sudo apt -t buster-backports search kicad

正在排序... 完成
全文搜索... 完成
horizon-eda/未知 0.20181108-1+b1 amd64
  EDA layout and schematic application

kicad/buster-backports 5.1.6+dfsg1-1~bpo10+1 amd64 [可从该版本升级：5.0.2+dfsg1-1]
  Electronic schematic and PCB design software

kicad-common/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Old common files used by kicad - Transitional Package

kicad-demos/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all [可从该版本升级：5.0.2+dfsg1-1]
  Demo projects for kicad

kicad-doc-ca/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Catalan)

kicad-doc-de/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (German)

kicad-doc-en/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (English)

kicad-doc-es/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Spanish)

kicad-doc-fr/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (French)

kicad-doc-id/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Indonesian)

kicad-doc-it/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Italian)

kicad-doc-ja/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Japanese)

kicad-doc-nl/未知,未知 5.0.2+dfsg1-1 all
  Kicad help files (Dutch)

kicad-doc-pl/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Polish)

kicad-doc-ru/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Russian)

kicad-doc-zh/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all
  Kicad help files (Chinese)

kicad-footprints/buster-backports,buster-backports 5.1.6-1~bpo10+1 all [可从该版本升级：5.0.2-1]
  Footprint symbols for KiCad's Pcbnew

kicad-libraries/buster-backports,buster-backports 5.1.6+dfsg1-1~bpo10+1 all [可从该版本升级：5.0.2+dfsg1-1]
  Virtual package providing common used libraries by kicad

kicad-packages3d/buster-backports,buster-backports 5.1.6-1~bpo10+1 all
  3D models for 3D viewer in KiCad's Pcbnew and Footprint Editor

kicad-pcb/stable 5.0.2+dfsg1-4 amd64
  Electronic schematic and PCB design software

kicad-symbols/buster-backports,buster-backports 5.1.6-1~bpo10+1 all [可从该版本升级：5.0.2-1]
  Schematic symbols for KiCad's Eeschema

kicad-templates/buster-backports,buster-backports 5.1.6-1~bpo10+1 all [可从该版本升级：5.0.2-1]
  Project templates for KiCad

pcb-rnd/未知 2.1.1-1 amd64
  Modular Printed Circuit Board layout tool


```

- 安装 `Debian` `backports` 源中的 `KiCad`


```bash
# 在 Debian backports 中安装 KiCad
sudo apt -t buster-backports install kicad

正在读取软件包列表... 完成
正在分析软件包的依赖关系树
正在读取状态信息... 完成
下列软件包是自动安装的并且现在不需要了：
  libgl2ps1.4 liboce-foundation11 liboce-modeling11 liboce-ocaf-lite11 liboce-ocaf11
  liboce-visualization11 libwxgtk3.0-0v5
使用'sudo apt autoremove'来卸载它(它们)。
将会同时安装下列软件：
  libocct-data-exchange-7.3 libocct-foundation-7.3 libocct-modeling-algorithms-7.3
  libocct-modeling-data-7.3 libocct-ocaf-7.3 libocct-visualization-7.3 libtbb2 libwxgtk3.0-gtk3-0v5
  python3-six python3-wxgtk4.0
建议安装：
  extra-xdg-menus kicad-doc-ca | kicad-doc-de | kicad-doc-en | kicad-doc-es | kicad-doc-fr | kicad-doc-id
  | kicad-doc-it | kicad-doc-ja | kicad-doc-pl | kicad-doc-ru | kicad-doc-zh kicad-packages3d wx3.0-doc
下列【新】软件包将被安装：
  libocct-data-exchange-7.3 libocct-foundation-7.3 libocct-modeling-algorithms-7.3
  libocct-modeling-data-7.3 libocct-ocaf-7.3 libocct-visualization-7.3 libtbb2 libwxgtk3.0-gtk3-0v5
  python3-six python3-wxgtk4.0
下列软件包将被升级：
  kicad
升级了 1 个软件包，新安装了 10 个软件包，要卸载 0 个软件包，有 90 个软件包未被升级。
需要下载 53.5 MB 的归档。
解压缩后会消耗 145 MB 的额外空间。
您希望继续执行吗？ [Y/n]
获取:1 https://community-packages.deepin.com/deepin apricot/main amd64 python3-six all 1.12.0-1 [15.7 kB]
获取:2 https://mirrors.tuna.tsinghua.edu.cn/debian buster-backports/main amd64 kicad amd64 5.1.6+dfsg1-1~bpo10+1 [23.7 MB]
获取:3 https://community-packages.deepin.com/deepin apricot/main amd64 libwxgtk3.0-gtk3-0v5 amd64 3.0.4+dfsg-8 [4,350 kB]
获取:4 https://community-packages.deepin.com/deepin apricot/main amd64 python3-wxgtk4.0 amd64 4.0.4+dfsg-2 [6,648 kB]
获取:5 https://community-packages.deepin.com/deepin apricot/main amd64 libtbb2 amd64 2018~U6-4 [148 kB]
获取:6 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-foundation-7.3 amd64 7.3.0+dfsg1-5 [1,272 kB]
获取:7 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-modeling-data-7.3 amd64 7.3.0+dfsg1-5 [2,660 kB]
获取:8 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-modeling-algorithms-7.3 amd64 7.3.0+dfsg1-5 [7,974 kB]
获取:9 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-visualization-7.3 amd64 7.3.0+dfsg1-5 [1,599 kB]
获取:10 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-ocaf-7.3 amd64 7.3.0+dfsg1-5 [1,200 kB]
获取:11 https://community-packages.deepin.com/deepin apricot/main amd64 libocct-data-exchange-7.3 amd64 7.3.0+dfsg1-5 [3,949 kB]
已下载 53.5 MB，耗时 11秒 (4,837 kB/s)
正在选中未选择的软件包 python3-six。
(正在读取数据库 ... 系统当前共安装有 196012 个文件和目录。)
准备解压 .../00-python3-six_1.12.0-1_all.deb  ...
正在解压 python3-six (1.12.0-1) ...
正在选中未选择的软件包 libwxgtk3.0-gtk3-0v5:amd64。
准备解压 .../01-libwxgtk3.0-gtk3-0v5_3.0.4+dfsg-8_amd64.deb  ...
正在解压 libwxgtk3.0-gtk3-0v5:amd64 (3.0.4+dfsg-8) ...
正在选中未选择的软件包 python3-wxgtk4.0。
准备解压 .../02-python3-wxgtk4.0_4.0.4+dfsg-2_amd64.deb  ...
正在解压 python3-wxgtk4.0 (4.0.4+dfsg-2) ...
正在选中未选择的软件包 libtbb2:amd64。
准备解压 .../03-libtbb2_2018~U6-4_amd64.deb  ...
正在解压 libtbb2:amd64 (2018~U6-4) ...
正在选中未选择的软件包 libocct-foundation-7.3:amd64。
准备解压 .../04-libocct-foundation-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-foundation-7.3:amd64 (7.3.0+dfsg1-5) ...
正在选中未选择的软件包 libocct-modeling-data-7.3:amd64。
准备解压 .../05-libocct-modeling-data-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-modeling-data-7.3:amd64 (7.3.0+dfsg1-5) ...
正在选中未选择的软件包 libocct-modeling-algorithms-7.3:amd64。
准备解压 .../06-libocct-modeling-algorithms-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-modeling-algorithms-7.3:amd64 (7.3.0+dfsg1-5) ...
正在选中未选择的软件包 libocct-visualization-7.3:amd64。
准备解压 .../07-libocct-visualization-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-visualization-7.3:amd64 (7.3.0+dfsg1-5) ...
正在选中未选择的软件包 libocct-ocaf-7.3:amd64。
准备解压 .../08-libocct-ocaf-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-ocaf-7.3:amd64 (7.3.0+dfsg1-5) ...
正在选中未选择的软件包 libocct-data-exchange-7.3:amd64。
准备解压 .../09-libocct-data-exchange-7.3_7.3.0+dfsg1-5_amd64.deb  ...
正在解压 libocct-data-exchange-7.3:amd64 (7.3.0+dfsg1-5) ...
准备解压 .../10-kicad_5.1.6+dfsg1-1~bpo10+1_amd64.deb  ...
正在解压 kicad (5.1.6+dfsg1-1~bpo10+1) 并覆盖 (5.0.2+dfsg1-1) ...
正在设置 libtbb2:amd64 (2018~U6-4) ...
正在设置 libwxgtk3.0-gtk3-0v5:amd64 (3.0.4+dfsg-8) ...
正在设置 python3-six (1.12.0-1) ...
正在设置 libocct-foundation-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 python3-wxgtk4.0 (4.0.4+dfsg-2) ...
正在设置 libocct-modeling-data-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 libocct-modeling-algorithms-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 libocct-visualization-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 libocct-ocaf-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 libocct-data-exchange-7.3:amd64 (7.3.0+dfsg1-5) ...
正在设置 kicad (5.1.6+dfsg1-1~bpo10+1) ...
正在处理用于 desktop-file-utils (0.23-4) 的触发器 ...
正在处理用于 mime-support (3.62) 的触发器 ...
正在处理用于 hicolor-icon-theme (0.17-2) 的触发器 ...
正在处理用于 lastore-daemon (5.0.6-1) 的触发器 ...
正在处理用于 libc-bin (2.28.7-1+deepin) 的触发器 ...
正在处理用于 man-db (2.8.5-2) 的触发器 ...
正在处理用于 shared-mime-info (1.10-1) 的触发器 ...
正在处理用于 bamfdaemon (0.5.4-1) 的触发器 ...
Rebuilding /usr/share/applications/bamf-2.index...

```



## [KiCad 文档](./KiCad_doc.md)
