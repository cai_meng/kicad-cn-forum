@[toc]
## [KiCad 插件](./KiCad_plugin.md)

## [KiCad 中国群文件](./KiCad_CN_group_files.md)

## 说明

- 本仓库使用 `git submodule` 方式储存 KiCad 中国群文件
- 使用 `工蜂` Git 服务，下载仓库需要登录腾讯云帐号（使用微信或 QQ 登录后修改用户名和密码，等会初始化子仓库需要使用腾讯云帐号和密码）。
- 如果在登录过程中出现用户名和密码输入错误，请使用下面 `Git` 命令行清除已登录的信息：

```bash
git config --system --unset credential.helper
```

## 教程

使用子模块仓库

```bash
// 初始化子模块仓库
git submodule init 

// 更新子模块仓库
git submodule update
```

或：

```bash
// 初始化和更新子模块仓库组合命令
git submodule update --init --recursive
```

从子模块的远端仓库更新并合并到本仓库

```bash
// 从子模块远端仓库更新并合并
git submodule update --remote --merge
```

# KiCad_CN 文件列表

访问 KiCad_CN 群文件仓库下的 `[KiCad_CN_index.md](https://git.code.tencent.com/taotieren/KiCad_CN/blob/master/KiCad_CN_index.md)` 文件查看最新文件列表。

## [KiCad 与其他 EDA 转换](./KiCad_2_other_EDA.md)