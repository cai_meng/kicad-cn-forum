@[toc]

## [KiCad 简介](./KiCad_description.md)

[KiCad](http://kicad.org/) 一个跨平台的开源电子设计自动化套件。

[KiCad 介绍](https://www.cnblogs.com/F4NNIU/p/KiCad.html)：

- KiCad EDA 是一款用于印刷电路板设计的开源自由软件，最初由法国人 Jean-Pierre Charras 于 1992 年推出，现由 KiCad 开发团队维护。
- KiCad 目前支持英语、法语、德语、意大利语、中文、日语、韩语等 22 种语言版本。
- 软件包含原理图设计、线路板绘制、符号库设计、封装库设计、线路板 3D 显示、Gerber 查看、线路板实用计算等工具。
- 官网地址：http://www.kicad-pcb.org/
- KiCad EDA 捐赠地址： https://cernandsocietyfoundation.cern/projects/kicad-development
- 2013 年 CERN（欧洲核子研究组织）的 BE-CO-HT 部门开始贡献一些资源支持其成为开源硬件领域与商用的 EDA 工具相当的工具软件。
- 软件主要开发语言：C++、 Python。

### Eeschema 功能

#### 原理图设计

原理图设计是高效的，具有您可以期望完成此类任务的所有工具。界面侧重于生产力。没有复杂性限制，因为大型设计可以划分为分层子表。提供各种导出选项（PDF，SVG，HPGL，Postscript）。

#### 电气规则检查 (ERC)

电气规则检查（ERC）会自动验证您的原理图连接。它检查输出引脚冲突，缺少驱动器和未连接的引脚。

#### 导出网表

导出网表格式包括：
- PSPICE
- CADSTAR
- PcbNew
- “通用” XML

#### 物料清单 (BOM)

BOM 生成可通过 Python 脚本或 XSLT 进行扩展，这允许许多可配置的格式。

#### 综合库

KiCad 自带了大量的符号，封装和匹配的 3D 模型库。他们是社区保持，所以他们永远不会停止改善。

### PcbNew 功能

#### 推挤布线

这个现代工具将帮助您更快地布局您的电路板。当您绘制轨道时，它会向前推进路径中的轨迹。如果无法做到这一点，您的轨迹将在障碍物周围重新布线。推挤和推挤布线器将确保您的 DRC 约束得到遵守。

#### 布线长度调整

使用专用工具调整高速设计的走线长度。

### 3D 视图功能

在布置 PCB 时，您可以在交互式 3D 视图中即时检查您的设计。旋转和平移以比 2D 显示更容易检查细节。

## [KiCad 安装](./KiCad_install.md)
