@[toc]

## [KiCad 中国群文件](./KiCad_CN_group_files.md)

## [KiCad 下载](./KiCad_downloads.md)

## [清华大学 TUNA 协会](https://mirrors.tuna.tsinghua.edu.cn/kicad/)

## [莞工 GNU/Linux 协会](https://mirrors.dgut.edu.cn/kicad/)

## [重庆大学开源镜像站](https://mirrors.cqu.edu.cn/kicad/)

## [哈尔滨工业大学开源镜像站](http://run.hit.edu.cn/kicad)

## [北京外国语大学开源软件镜像站](https://mirrors.bfsu.edu.cn/kicad/)

## [开源软件镜像站](https://mirrors.cnnic.cn/)

## [阿里云镜像站](https://mirrors.aliyun.com/kicad/) 